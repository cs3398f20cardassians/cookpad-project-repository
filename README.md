# CookPad

> Created by: Tanya Garduno, Allison Spitzenberger, Julian Sherlock, Connor Scott, Brandon Burtchell

## Table of contents

* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Individual Features](#individual-features)
* [Project Status](#project-status)

## General info

![Logo](./images/logo.png)

What we're creating:

* We are creating a recipe application called “CookPad.” Our goal right now is to create a program but long term would like to do a website. CookPad will allow users to add recipes from their pantry and get recipes based upon the ingredients that the user has available.

Who we're doing it for:

* We're doing things for people that are inexperienced in the kitchen, people wanting to explore new recipes, or people that would like to use certain ingredients.

Why we're doing this:

* We want the cooking process to be less intimidating. The aspect so many people love about being in the kitchen is advancing new skills and trying new things. 
* We want to enable our users to do this without getting bogged down in the logistics of cooking; keeping up with what you have and what you need, keeping track of recipe books etc.
* We believe that if we can allow the user to save time on the less interesting sides of cooking, they'll be more comfortable trying new things and being in the kitchen more.

## Screenshots

Home Menu:

![Home](./images/home.PNG)

Pantry:

![Pantry](./images/pantry.PNG)

Recipe List:

![Recipe List](./images/recipelist.PNG)

A Recipe:

![Recipe](./images/spaghetti.PNG)

## Technologies

* [Java](https://go.java/)
* [StarUML](http://staruml.io/)
* [Figma](https://www.figma.com/)
* [Photoshop](https://www.adobe.com/products/photoshop.html)

## Setup

Navigate to:
```
./src/main/java/cookpad
```

Compile and run using the following commands:
```
javac -d classes *.java
java -cp classes cookpad.CookPadMain
```

## Features

Features ready:

- Recipe management (Create, delete, view, sort, etc.).
- Pantry management (Create, delete, view, sort, etc.).
- A working GUI.
- CircleCi and Gradle implementation

## Individual Features

> For Assignment 22.

###### Brandon Burtchell

- With Connor's help, we rewrote the [RecipeManager](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/RecipeManager.java) and [Recipe](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/Recipe.java) classes to match the needs of the GUI-side of the program in the [feature/recipe-manager-refactoring](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/commits/branch/feature%2Frecipe-manager-refactoring) branch.
- Additionally, I helped in the [IngredientManager](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/IngredientManager.java) and [Ingredient](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/Ingredient.java) class refactoring with Tanya and Allison in this [commit](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/commits/d381bd05d7c586d8d191dfe9ad066b330e365fdc). 
- Individually, I created a new component for the GUI: the [EditRecipeGUI](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/EditRecipeGUI.java) class. This component was made to handle the creation or the editing of a Recipe object. Development can be found in the [feature/add-edit-recipe-panel](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/commits/branch/feature%2Fadd-edit-recipe-panel) branch.
- To put everything together, I updated the [CookPadController](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/CookPadController.java) to handle all actions for every GUI component in the view, in accordance with MVC. This file was updated periodically on multiple branches, so there isn't a specific branch tied to its development.

###### Julian Sherlock

- See [CircleCi test](https://app.circleci.com/pipelines/bitbucket/cs3398f20cardassians/cookpad-project-repository/203/workflows/5fc46d80-ccb0-4b76-be7c-c99396a8c84f/jobs/204), [test files](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/development/src/test/java/cookpad/)
- This sprint, my focus was on getting Gradle up and running, then integrating it with CircleCi. This was achieved with invaluable help from Dr. Lehr. 
- From there I wrote test files for 4 of our files; Recipe, RecipeManager, Ingredient and IngredientManager. The focus was on these files specifically
  because the other files were seeing a great deal of rapid, unforeseen changes during this sprint, so any tests would have likely unworkable until
  the files were at a consistent state. That being said, a number of changes were made to the files I did make tests for, which required 
  re-writing of a large portion of tests anyway, a task made easier by my teammate's understanding of their individual classes, who I consulted for help.

###### Allison Spitzenberger

- [IngredientManager](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/IngredientManager.java) and [Ingredient](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/main/java/cookpad/Ingredient.java) class refactoring with Tanya and Allison in this [commit](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/commits/d381bd05d7c586d8d191dfe9ad066b330e365fdc). 
- I updated the IngredientManager.java file to help adapt to the refactoring, which is the GUI. 
- I also helped update the FileManager.java, with Connor, and PantryGUI.java, with Tanya.

###### Connor Scott

- See [Recipe.java](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/cookpad/Recipe.java) and [RecipeManager.java](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/cookpad/RecipeManager.java) and [FileManager.java](https://bitbucket.org/cs3398f20cardassians/cookpad-project-repository/src/master/src/cookpad/FileManager.java)
- Refactoring of Recipe.java, RecipeManager.java, and several changes to FileManager.java that would ensure the correct reading and writing to the files. 
- Helped Brandon integrate the functions in RecipeManager.java with the GUI, which worked well with minor issues.

###### Tanya Garduno

- PantryGUI and ingredient manager back end code integrated. 

## Project Status
Project is: _completed_.