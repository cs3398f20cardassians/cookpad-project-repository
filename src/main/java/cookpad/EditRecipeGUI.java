package cookpad;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

/**
 * The Recipe-editing screen. Shown when a user adds or edits a preexisting recipe.
 * 
 * @author Brandon Burtchell
 */
public class EditRecipeGUI {
  public static final String CANCEL_RECIPE_EDIT_BUTTON_NAME = "Cancel and Return";
  public static final String CONFIRM_RECIPE_BUTTON_NAME = "Confirm Changes";

  // swing components:
  private JPanel panel;
  private JLabel recipeNameLabel;
  private JLabel categoryLabel;
  private JLabel ingredientsLabel;
  private JLabel recipeContentLabel;
  private JTextField recipeNameField;
  private JComboBox<String> categoryField;
  private JTextArea ingredientsField;
  private JTextArea recipeContentField;
  private JScrollPane ingredientsScrollPane;
  private JScrollPane recipeContentScrollPane;
  private JButton cancelButton;
  private JButton confirmButton;

  public EditRecipeGUI() {
    // instantiate swing components:
    panel = new JPanel();
    recipeNameLabel = new JLabel("Recipe Name");
    categoryLabel = new JLabel("Category");
    ingredientsLabel = new JLabel("Ingredients");
    recipeContentLabel = new JLabel("Instructions");
    recipeNameField = new JTextField();
    categoryField = new JComboBox<>(RecipeManager.RECIPE_CATEGORIES);
    ingredientsField = new JTextArea();
    recipeContentField = new JTextArea();
    ingredientsScrollPane = new JScrollPane();
    recipeContentScrollPane = new JScrollPane();
    cancelButton = new JButton(CANCEL_RECIPE_EDIT_BUTTON_NAME);
    confirmButton = new JButton(CONFIRM_RECIPE_BUTTON_NAME);

    // panel setup:
    panel.setPreferredSize(WindowGUI.DEFAULT_SCREEN_SIZE);
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    // setup category combo box:
    categoryField.setSelectedIndex(0); // sets default category

    // text areas setup:
    ingredientsField.setEditable(true);
    ingredientsField.setLineWrap(true);
    ingredientsField.setWrapStyleWord(true);
    recipeContentField.setEditable(true);
    recipeContentField.setLineWrap(true);
    recipeContentField.setWrapStyleWord(true);
    
    // scroll panes setup:
    ingredientsScrollPane.setViewportView(ingredientsField);
    recipeContentScrollPane.setViewportView(recipeContentField);

    // setting fonts:
    recipeNameLabel.setFont(WindowGUI.BUTTON_FONT);
    categoryLabel.setFont(WindowGUI.BUTTON_FONT);
    ingredientsLabel.setFont(WindowGUI.BUTTON_FONT);
    recipeContentLabel.setFont(WindowGUI.BUTTON_FONT);
    recipeNameField.setFont(WindowGUI.BUTTON_FONT);
    categoryField.setFont(WindowGUI.BUTTON_FONT);
    ingredientsField.setFont(WindowGUI.BUTTON_FONT);
    recipeContentField.setFont(WindowGUI.BUTTON_FONT);
    cancelButton.setFont(WindowGUI.BUTTON_FONT);
    confirmButton.setFont(WindowGUI.BUTTON_FONT);

    // align components:
    recipeNameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    categoryLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    ingredientsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    recipeContentLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    recipeNameField.setAlignmentX(Component.CENTER_ALIGNMENT);
    categoryField.setAlignmentX(Component.CENTER_ALIGNMENT);
    ingredientsField.setAlignmentX(Component.CENTER_ALIGNMENT);
    recipeContentField.setAlignmentX(Component.CENTER_ALIGNMENT);
    cancelButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    confirmButton.setAlignmentX(Component.CENTER_ALIGNMENT);

    // add panel components:
    panel.add(recipeNameLabel);
    panel.add(recipeNameField);
    panel.add(categoryLabel);
    panel.add(categoryField);
    panel.add(ingredientsLabel);
    panel.add(ingredientsScrollPane);
    panel.add(recipeContentLabel);
    panel.add(recipeContentScrollPane);
    panel.add(cancelButton);
    panel.add(confirmButton);
  }

  public JPanel getPanel() {
    return panel;
  }

  public void setRecipeName(String name) {
    recipeNameField.setText(name);
    panel.revalidate();
  }

  public void setCategory(String type) {
    for (String s : RecipeManager.RECIPE_CATEGORIES) {
      if (s.equals(type)) {
        categoryField.setSelectedItem(type);
        panel.revalidate();
        return;
      }
    }
    categoryField.setSelectedIndex(0);
  }

  public void setCategory(int index) {
    if (index < RecipeManager.RECIPE_CATEGORIES.length) {
      categoryField.setSelectedIndex(index);
    } else {
      categoryField.setSelectedIndex(0);
    }
  }

  public void setIngredients(ArrayList<String> text) {
    String newText = "";
    for (int i = 0; i < text.size(); i++) {
      newText.concat(newText + text.get(i) + "\n");
    }
    ingredientsField.setText(newText);
    panel.revalidate();
  }

  public void setRecipeContent(String text) {
    recipeContentField.setText(text);
    panel.revalidate();
  }

  public String getRecipeNameField() {
    return recipeNameField.getText();
  }

  public String getCategoryField() {
    return (String)categoryField.getSelectedItem();
  }
 
  /**
   * Returns an array of each line in the ingredients field.
   */
  public String[] getIngredientsField() {
    return ingredientsField.getText().split(System.getProperty("line.separator"));
  }

  public String getRecipeContentField() {
    return recipeContentField.getText();
  }

  public JButton getCancelButton() {
    return cancelButton;
  }

  public JButton getConfirmButton() {
    return confirmButton;
  }
}