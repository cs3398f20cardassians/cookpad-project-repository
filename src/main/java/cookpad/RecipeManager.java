package cookpad;

import java.util.*;
import java.io.IOException;

/**
 * @author Connor Scott
 * @author Brandon Burtchell
 */
public class RecipeManager {
  private static ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
  public static final String[] RECIPE_CATEGORIES = { "Breakfast", "Lunch", "Dinner", "Dessert", "Other" };

  public RecipeManager() {
    // TODO: initialize the recipe list with saved file's info on startup.
  }

  public static ArrayList<Recipe> getRecipeList() {
    return recipeList;
  }

  public static ArrayList<Recipe> setRecipeList(ArrayList<Recipe> list) {
    recipeList = list;
    return recipeList;
  }

  public void addRecipe(Recipe r) throws IOException {
    recipeList.add(r);
    sortRecipeByCategory(); // FIXME: should this be called in controller or here?
    FileManager.recipeWriter.flush();
  }

  public void deleteRecipe(Recipe r) throws IOException {
    if (recipeList.contains(r)) {
      recipeList.remove(r);
    }
    FileManager.recipeWriter.flush();
  }

  /**
   * Sorts from Breakfast to Other in order.
   * 
   * @throws IOException
   */
  public static void sortRecipeByCategory() throws IOException {
    ArrayList<Recipe> breakfastList = new ArrayList<Recipe>();
    ArrayList<Recipe> lunchList = new ArrayList<Recipe>();
    ArrayList<Recipe> dinnerList = new ArrayList<Recipe>();
    ArrayList<Recipe> dessertList = new ArrayList<Recipe>();
    ArrayList<Recipe> otherList = new ArrayList<Recipe>();
    ArrayList<Recipe> tempList = new ArrayList<Recipe>();

    for (int i = 0; i < recipeList.size(); i++) {
      if (recipeList.get(i).getType() == "Breakfast") {
        breakfastList.add(recipeList.get(i));
      } else if (recipeList.get(i).getType() == "Lunch") {
        lunchList.add(recipeList.get(i));
      } else if (recipeList.get(i).getType() == "Dinner") {
        dinnerList.add(recipeList.get(i));
      } else if (recipeList.get(i).getType() == "Dessert") {
        dessertList.add(recipeList.get(i));
      } else {
        otherList.add(recipeList.get(i));
      }
    }

    tempList.addAll(breakfastList);
    tempList.addAll(lunchList);
    tempList.addAll(dinnerList);
    tempList.addAll(dessertList);
    tempList.addAll(otherList);
    recipeList = tempList;
    FileManager.recipeWriter.flush();
  }
}