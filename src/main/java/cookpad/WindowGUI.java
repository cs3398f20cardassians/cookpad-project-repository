package cookpad;

import java.awt.*;
import javax.swing.*;

/**
 * The main view object of the program.
 * 
 * @author Brandon Burtchell
 */
public class WindowGUI {
  // constants for window creation:
  public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
  public static final int DEFAULT_WIDTH = (int) (SCREEN_SIZE.getWidth() * 1 / 4);
  public static final int DEFAULT_HEIGHT = (int) (SCREEN_SIZE.getHeight() * 2 / 3);
  public static final Dimension DEFAULT_SCREEN_SIZE = new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);

  // constants for panel names:
  public static final String HOME_PANEL_NAME = "Home Panel";
  public static final String RECIPE_LIST_PANEL_NAME = "Recipe List Panel";
  public static final String RECIPE_PANEL_NAME = "Recipe Panel";
  public static final String EDIT_RECIPE_PANEL_NAME = "Edit Recipe Panel";
  public static final String PANTRY_PANEL_NAME = "Pantry Panel";

  // constants for button names shared across multiple components:
  public static final String RETURN_HOME_BUTTON_NAME = "Return to Home Menu";

  // font constants:
  public static final Font LARGE_HEADER_FONT = new Font("Futura", Font.BOLD, 20);
  public static final Font BUTTON_FONT = new Font("Futura", Font.PLAIN, 14);

  // swing components:
  private JFrame window;
  private JPanel cards; // a "cardholder" panel that contains each of the app's panels

  // each of the app's view panels.
  private HomeGUI homePanel;
  private RecipeListGUI recipeListPanel;
  private RecipeGUI recipePanel;
  private EditRecipeGUI editRecipePanel;
  private PantryGUI pantryPanel;

  public WindowGUI() {
    // instantiate swing components:
    window = new JFrame("CookPad");
    cards = new JPanel(new CardLayout());
    homePanel = new HomeGUI();
    recipeListPanel = new RecipeListGUI();
    recipePanel = new RecipeGUI();
    editRecipePanel = new EditRecipeGUI();
    pantryPanel = new PantryGUI();

    // frame setup:
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setPreferredSize(DEFAULT_SCREEN_SIZE);
    window.setSize(DEFAULT_SCREEN_SIZE);
    window.setLocation((SCREEN_SIZE.width / 2) - (window.getSize().width / 2), (SCREEN_SIZE.height / 2) - (window.getSize().height / 2));
    window.setLayout(new BorderLayout()); // quick way to center pane contents

    // add each panel to the "cardholder" panel:
    cards.setPreferredSize(DEFAULT_SCREEN_SIZE);
    cards.add(homePanel.getPanel(), HOME_PANEL_NAME);
    cards.add(recipeListPanel.getPanel(), RECIPE_LIST_PANEL_NAME);
    cards.add(recipePanel.getPanel(), RECIPE_PANEL_NAME);
    cards.add(editRecipePanel.getPanel(), EDIT_RECIPE_PANEL_NAME);
    cards.add(pantryPanel.getPanel(), PANTRY_PANEL_NAME);
    
    // add the "cardholder" to the content pane:
    window.getContentPane().add(cards, BorderLayout.CENTER);

    // display the window:
    window.pack();
    window.setVisible(true);
  }

  public JPanel getCardsPanel() {
    return cards;
  }

  public HomeGUI getHomePanel() {
    return homePanel;
  }

  public RecipeListGUI getRecipeListPanel() {
    return recipeListPanel;
  }

  public RecipeGUI getRecipePanel() {
    return recipePanel;
  }

  public EditRecipeGUI getEditRecipePanel() {
    return editRecipePanel;
  }

  public PantryGUI getPantryPanel() {
    return pantryPanel;
  }
}
