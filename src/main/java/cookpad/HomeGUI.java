package cookpad;

import java.awt.*;
import javax.swing.*;

/**
 * Provides the construction of the Home panel.
 * 
 * @author Brandon Burtchell
 */
public class HomeGUI {
  // constants for home panel's button names:
  public static final String RECIPE_LIST_BUTTON_NAME = "Recipes";
  public static final String PANTRY_BUTTON_NAME = "Pantry";

  // swing components:
  private JPanel panel;
  private JLabel welcomeMessage;
  private JButton recipeButton;
  private JButton pantryButton;

  /**
   * Constructor for the Home panel view.
   */
  public HomeGUI() {
    // instantiate swing components:
    panel = new JPanel();
    welcomeMessage = new JLabel("Welcome to CookPad", SwingConstants.CENTER);
    recipeButton = new JButton(RECIPE_LIST_BUTTON_NAME);
    pantryButton = new JButton(PANTRY_BUTTON_NAME);

    // panel setup:
    panel.setPreferredSize(WindowGUI.DEFAULT_SCREEN_SIZE);
    panel.setLayout(new GridLayout(3, 1));

    // setting fonts:
    welcomeMessage.setFont(WindowGUI.LARGE_HEADER_FONT);
    recipeButton.setFont(WindowGUI.BUTTON_FONT);
    pantryButton.setFont(WindowGUI.BUTTON_FONT);

    // add panel components:
    panel.add(welcomeMessage);
    panel.add(recipeButton);
    panel.add(pantryButton);
  }

  public JPanel getPanel() {
    return panel;
  }

  public JButton getRecipeButton() {
    return recipeButton;
  }

  public JButton getPantryButton() {
    return pantryButton;
  }
}
