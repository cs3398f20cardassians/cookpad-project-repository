package cookpad;

import java.io.*;
import java.util.*;

/**
 * @author Allison
 * @author Connor
 */
public class FileManager {
  static FileInputStream ingredientIn;
  static FileOutputStream ingredientOut;
  static FileInputStream recipeIn;
  static FileOutputStream recipeOut;
  static File recipeFile;
  static File ingredientFile;
  static BufferedWriter recipeWriter;
  static BufferedWriter ingredientWriter;
  static BufferedReader recipeReader;
  static BufferedReader ingredientReader;

  public static void FileCreate() throws FileNotFoundException, IOException {
    IngredientCheck();
    RecipeCheck();
    readRecipeFile();
    readIngredientFile();
  }

  public static void IngredientCheck() throws FileNotFoundException, IOException {
    ingredientFile = new File("IngredientInventory.txt");
    if (!ingredientFile.exists()) {
      ingredientFile = new File("IngredientInventory.txt");
      ingredientFile.createNewFile();
    }
  }

  public static void RecipeCheck() throws FileNotFoundException, IOException {
    recipeFile = new File("RecipeInventory.txt");
    if (!recipeFile.exists()) {
      recipeFile = new File("RecipeInventory.txt");
      recipeFile.createNewFile();
    }
  }

  public static void RecipeWrite() throws FileNotFoundException, IOException {
    ArrayList<Recipe> list = RecipeManager.getRecipeList();
    recipeWriter = new BufferedWriter(new FileWriter(recipeFile));
    String formattedOutString = list.toString().replace("], ", "#").replace(", ", "")
        // .replace(",", "")
        .replace("[", "").replace("]", "").replace("Recipe:", "").replace("Category:", "").replace(" Ingredient:", "")
        .replace("Ingredient:", "").replace("Ingredients:", "").replace("Quantity:", "").replace("Instructions:", "")
        .replace("\t", "").trim();
    formattedOutString = "#\n" + formattedOutString + "\n#";
    recipeWriter.write(formattedOutString);
    recipeWriter.flush();
  }

  public static void IngredientWrite() throws FileNotFoundException, IOException {
    // Delete all .replace() methods to check format on what needs to be replaced
    // Start replace over starting with "[" , "]" and so on to get just the name
    // line by line.
    ArrayList<Ingredient> list = IngredientManager.officialList;
    ingredientWriter = new BufferedWriter(new FileWriter(ingredientFile));
    String formattedOutString = list.toString().replace(",", "").replace("[", "").replace("]", "")
        .replace("Recipe:", "").replace("Category:", "").replace(" Ingredient:", "").replace("Ingredient:", "")
        .replace("Quantity:", "").replace("Instructions:", "").replace("\t", "").trim();
    ingredientWriter.write(formattedOutString);
    ingredientWriter.flush();
  }

  public static void readIngredientFile() throws IOException {
    ArrayList<Ingredient> list = new ArrayList<>();

    ingredientReader = new BufferedReader(new FileReader(ingredientFile));
    while (ingredientReader.ready()) {
      String name = ingredientReader.readLine();
      // String userType= ingredientReader.readLine();
      // String amountString= ingredientReader.readLine();

      // int amount= Ingredient.checkUserValidation2(amountString);

      list.add(new Ingredient(name)); // , userType, amount));
    }

    IngredientManager.officialList = list;

  }

  public static void readRecipeFile() throws IOException {
    ArrayList<Recipe> list = new ArrayList<>();

    recipeReader = new BufferedReader(new FileReader(recipeFile));
    String ingName = "", recipeName = "", ingType = "", userTypeCat = "", instructions = "", amountString = "";
    // System.out.println("Test1");
    if (recipeReader.ready())
      recipeReader.readLine(); // skip first #

    while (recipeReader.ready()) // for recipes
    {
      ArrayList<Ingredient> recipeIngredList = new ArrayList<>();
      ArrayList<Ingredient> tempIngredList = new ArrayList<>();
      System.out.println("Recipe Created");

      recipeName = recipeReader.readLine();
      userTypeCat = recipeReader.readLine();
      instructions = recipeReader.readLine();

      String notPound = recipeReader.readLine();

      while ((!notPound.equals("#") && notPound != null) && recipeReader.ready()) { // for ingredients

        ingName = notPound;
        ingType = recipeReader.readLine();
        amountString = recipeReader.readLine();
        // int amount = Ingredient.checkUserValidation2(amountString);

        tempIngredList.add(new Ingredient(ingName));// , ingType, amount));
        System.out.println("Test2");
        notPound = recipeReader.readLine();
        System.out.println(notPound);
        if (notPound.equals("#")) {
          list.add(new Recipe(recipeName, userTypeCat, instructions, tempIngredList));
          // recipeIngredList = tempIngredList;
          break;
          // recipeReader.readLine();
        }
      }
      // Recipe.recipeIngredientList = recipeIngredList;

      // notPound = recipeReader.readLine();
    }

    // list.add(new Recipe(recipeName, userTypeCat, instructions,
    // Recipe.recipeIngredientList));

    RecipeManager.setRecipeList(list);
  }

  public static void fileClose() throws FileNotFoundException, IOException {
    if (recipeWriter != null) {
      RecipeWrite();
      recipeWriter.close();
    }

    if (ingredientWriter != null) {
      IngredientWrite();
      ingredientWriter.close();
    }
  }
}
