package cookpad;

import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

/**
 * Provides the view of an individual recipe.
 * 
 * @author Brandon Burtchell
 */
public class RecipeGUI {
  public static final String RETURN_RECIPE_LIST_BUTTON_NAME = "Return to Recipes List";

  // swing components:
  private JPanel panel;
  private JLabel recipeName;
  private JLabel recipeType;
  private JScrollPane ingredientsScrollPane;
  private JScrollPane recipeScrollPane;
  private JTextArea ingredientsField;
  private JTextArea recipeContent;
  private JButton returnRecipeListButton;

  public RecipeGUI() {
    // instantiate swing components:
    panel = new JPanel();
    recipeName = new JLabel();
    recipeType = new JLabel();
    ingredientsScrollPane = new JScrollPane();
    recipeScrollPane = new JScrollPane();
    ingredientsField = new JTextArea();
    recipeContent = new JTextArea();
    returnRecipeListButton = new JButton(RETURN_RECIPE_LIST_BUTTON_NAME);

    // panel setup:
    panel.setPreferredSize(WindowGUI.DEFAULT_SCREEN_SIZE);
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    // text area setup:
    ingredientsField.setEditable(false);
    ingredientsField.setLineWrap(true);
    ingredientsField.setWrapStyleWord(true);
    recipeContent.setEditable(false);
    recipeContent.setLineWrap(true);
    recipeContent.setWrapStyleWord(true);

    // scroll pane setup:
    ingredientsScrollPane.setViewportView(ingredientsField);
    recipeScrollPane.setViewportView(recipeContent);

    // setting fonts:
    recipeName.setFont(WindowGUI.LARGE_HEADER_FONT);
    ingredientsField.setFont(WindowGUI.BUTTON_FONT);
    recipeContent.setFont(WindowGUI.BUTTON_FONT);
    returnRecipeListButton.setFont(WindowGUI.BUTTON_FONT);

    // align components:
    recipeName.setAlignmentX(Component.CENTER_ALIGNMENT);
    recipeType.setAlignmentX(Component.CENTER_ALIGNMENT);
    ingredientsScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
    recipeScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
    returnRecipeListButton.setAlignmentX(Component.CENTER_ALIGNMENT);

    // add panel components:
    panel.add(recipeName);
    panel.add(recipeType);
    panel.add(ingredientsScrollPane);
    panel.add(recipeScrollPane);
    panel.add(returnRecipeListButton);
  }

  public JPanel getPanel() {
    return panel;
  }

  public void setRecipeName(String name) {
    recipeName.setText(name);
    panel.revalidate();
  }

  public void setRecipeType(String type) {
    recipeType.setText(type);
    panel.revalidate();
  }

  public void setRecipeContent(String text) {
    recipeContent.setText(text);
  }

  public void setIngredients(ArrayList<String> text) {
    String newText = "";
    for (int i = 0; i < text.size(); i++) {
      newText.concat(newText + text.get(i) + "\n");
    }
    ingredientsField.setText(newText);
    panel.revalidate();
  }

  public JButton getReturnRecipeListButton() {
    return returnRecipeListButton;
  }
}
