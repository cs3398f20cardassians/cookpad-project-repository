package cookpad;

import java.awt.*;
import javax.swing.*;

/**
 * Provides the view of the list of recipes.
 * 
 * @author Brandon Burtchell
 */
public class RecipeListGUI {
  // constants for the recipe list panel's button names:
  public static final String ADD_RECIPE_BUTTON_NAME = "Add Recipe";
  public static final String REMOVE_RECIPE_BUTTON_NAME = "Remove Recipe";
  public static final String VIEW_RECIPE_BUTTON_NAME = "View Recipe";
  public static final String EDIT_RECIPE_BUTTON_NAME = "Edit Selected Recipe";

  // swing components:
  private JPanel panel;
  private JLabel chooseRecipeMessage;
  private JList<String> recipeList;
  private DefaultListModel<String> recipeListModel;
  private JScrollPane scrollPane;
  private JButton addRecipeButton;
  private JButton removeRecipeButton;
  private JButton returnHomeButton;
  private JButton viewRecipeButton;
  private JButton editRecipeButton;

  /**
   * Constructor for the Recipe list view.
   */
  public RecipeListGUI() {
    // instantiate swing components:
    panel = new JPanel();
    chooseRecipeMessage = new JLabel("Recipes", SwingConstants.CENTER);
    recipeList = new JList<String>();
    recipeListModel = new DefaultListModel<String>();
    scrollPane = new JScrollPane();
    viewRecipeButton = new JButton(VIEW_RECIPE_BUTTON_NAME);
    addRecipeButton = new JButton(ADD_RECIPE_BUTTON_NAME);
    removeRecipeButton = new JButton(REMOVE_RECIPE_BUTTON_NAME);
    editRecipeButton = new JButton(EDIT_RECIPE_BUTTON_NAME);
    returnHomeButton = new JButton(WindowGUI.RETURN_HOME_BUTTON_NAME);

    // panel setup:
    panel.setPreferredSize(WindowGUI.DEFAULT_SCREEN_SIZE);
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    // list setup:
    recipeList.setModel(recipeListModel);

    // scroll pane setup:
    scrollPane.setViewportView(recipeList);

    // setting fonts:
    chooseRecipeMessage.setFont(WindowGUI.LARGE_HEADER_FONT);
    recipeList.setFont(WindowGUI.BUTTON_FONT);
    viewRecipeButton.setFont(WindowGUI.BUTTON_FONT);
    addRecipeButton.setFont(WindowGUI.BUTTON_FONT);
    removeRecipeButton.setFont(WindowGUI.BUTTON_FONT);
    editRecipeButton.setFont(WindowGUI.BUTTON_FONT);
    returnHomeButton.setFont(WindowGUI.BUTTON_FONT);

    // align components:
    chooseRecipeMessage.setAlignmentX(Component.CENTER_ALIGNMENT);
    scrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
    viewRecipeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    addRecipeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    removeRecipeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    editRecipeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    returnHomeButton.setAlignmentX(Component.CENTER_ALIGNMENT);

    // add panel components:
    panel.add(chooseRecipeMessage);
    panel.add(scrollPane);
    panel.add(viewRecipeButton);
    panel.add(addRecipeButton);
    panel.add(removeRecipeButton);
    panel.add(editRecipeButton);
    panel.add(returnHomeButton);
  }

  public JPanel getPanel() {
    return panel;
  }

  public void setRecipeListModel(DefaultListModel<String> newListModel) {
    recipeListModel = newListModel;
    recipeList.setModel(recipeListModel);
    panel.revalidate();
  }

  public String getSelectedRecipe() {
    return recipeListModel.getElementAt(recipeList.getSelectedIndex());
  }

  public JButton getViewRecipeButton() {
    return viewRecipeButton;
  }

  public JButton getAddRecipeButton() {
    return addRecipeButton;
  }

  public JButton getRemoveRecipeButton() {
    return removeRecipeButton;
  }

  public JButton getEditRecipeButton() {
    return editRecipeButton;
  }

  public JButton getReturnHomeButton() {
    return returnHomeButton;
  }
}
