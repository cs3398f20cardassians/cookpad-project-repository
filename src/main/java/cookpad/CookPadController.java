package cookpad;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.IOException;
import javax.swing.*;

/**
 * Cookpad's main controller. Communicates with the data and view objects of the
 * application.
 * 
 * @author Brandon Burtchell
 * @author tagarduno
 */
public class CookPadController implements ActionListener {
  // data objects:
  private static RecipeManager rm;
  private static IngredientManager im;

  // view object:
  private static WindowGUI view;
  private static CardLayout c;

  public CookPadController() {
    super();

    // insantiate application objects:
    rm = new RecipeManager();
    im = new IngredientManager();
    view = new WindowGUI();
    c = (CardLayout) (view.getCardsPanel().getLayout());

    try {
      FileManager.FileCreate();
    } catch(Exception e) {}

    addActionListeners();
  }

  /**
   * Adds this action listener object to each component in the view.
   */
  private void addActionListeners() {
    // home panel buttons:
    view.getHomePanel().getRecipeButton().addActionListener(this);
    view.getHomePanel().getPantryButton().addActionListener(this);

    // recipe list panel buttons:
    view.getRecipeListPanel().getViewRecipeButton().addActionListener(this);
    view.getRecipeListPanel().getAddRecipeButton().addActionListener(this);
    view.getRecipeListPanel().getRemoveRecipeButton().addActionListener(this);
    view.getRecipeListPanel().getEditRecipeButton().addActionListener(this);
    view.getRecipeListPanel().getReturnHomeButton().addActionListener(this);

    // recipe panel buttons:
    view.getRecipePanel().getReturnRecipeListButton().addActionListener(this);

    // edit recipe panel buttons:
    view.getEditRecipePanel().getCancelButton().addActionListener(this);
    view.getEditRecipePanel().getConfirmButton().addActionListener(this);

    // pantry panel buttons:
    view.getPantryPanel().getAddIngredientButton().addActionListener(this);
    view.getPantryPanel().getDeleteIngredientButton().addActionListener(this);
    view.getPantryPanel().getReturnHomeButton().addActionListener(this);
  }

  /**
   * Handles the functionality of the program.
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    String actionName = e.getActionCommand();

    // handle card panel switching:
    if (actionName == HomeGUI.RECIPE_LIST_BUTTON_NAME) {
      updateRecipeList();
      c.show(view.getCardsPanel(), WindowGUI.RECIPE_LIST_PANEL_NAME);
    } else if (actionName == HomeGUI.PANTRY_BUTTON_NAME) {
      updateIngredientList();
      c.show(view.getCardsPanel(), WindowGUI.PANTRY_PANEL_NAME);
    } else if (actionName == WindowGUI.RETURN_HOME_BUTTON_NAME) {
      c.show(view.getCardsPanel(), WindowGUI.HOME_PANEL_NAME);
    }

    // handle recipeListPanel buttons:
    else if (actionName == RecipeListGUI.VIEW_RECIPE_BUTTON_NAME) {
      try {
        String selection = view.getRecipeListPanel().getSelectedRecipe();
        displayRecipe(selection); // displays the selected recipe
      } catch (ArrayIndexOutOfBoundsException a) {} // do nothing if a recipe isn't selected
    } else if (actionName == RecipeListGUI.EDIT_RECIPE_BUTTON_NAME) {
      try {
        String selection = view.getRecipeListPanel().getSelectedRecipe();
        editRecipe(selection); // displays the selected recipe
      } catch (ArrayIndexOutOfBoundsException a) {} // do nothing if a recipe isn't selected
    } else if (actionName == RecipeListGUI.ADD_RECIPE_BUTTON_NAME) {
      showEmptyRecipe(); // prompts user to create a new recipe
    } else if (actionName == RecipeListGUI.REMOVE_RECIPE_BUTTON_NAME) {
      try {
        String selection = view.getRecipeListPanel().getSelectedRecipe();
        removeRecipe(selection); // prompts user to remove a selected recipe and update the model
      } catch (ArrayIndexOutOfBoundsException a) {} // do nothing if a recipe isn't selected
    }

    // handle recipePanel buttons:
    else if (actionName == RecipeGUI.RETURN_RECIPE_LIST_BUTTON_NAME) {
      c.show(view.getCardsPanel(), WindowGUI.RECIPE_LIST_PANEL_NAME);
    }

    // handle editRecipePanel buttons:
    else if (actionName == EditRecipeGUI.CANCEL_RECIPE_EDIT_BUTTON_NAME) {
      updateRecipeList();
      c.show(view.getCardsPanel(), WindowGUI.RECIPE_LIST_PANEL_NAME);
    } else if (actionName == EditRecipeGUI.CONFIRM_RECIPE_BUTTON_NAME) {
      saveEditedRecipe();
      updateRecipeList();
      c.show(view.getCardsPanel(), WindowGUI.RECIPE_LIST_PANEL_NAME);
    }

    // handle pantryPanel buttons:
    else if (actionName == PantryGUI.DELETE_INGREDIENT_BUTTON_NAME) {
      try {
        String selection = view.getPantryPanel().getSelectedIngredient();
        deleteIngredient(selection);
      } catch (ArrayIndexOutOfBoundsException a) {}
    } else if (actionName == PantryGUI.ADD_INGREDIENT_BUTTON_NAME) {
      addIngredient();
    }
  }

  /**
   * Updates the GUI with the newest RecipeManager data.
   */
  private void updateRecipeList() {
    DefaultListModel<String> newListModel = new DefaultListModel<String>();
    for (Recipe r : rm.getRecipeList()) {
      newListModel.addElement(r.getRecipe());
    }
    view.getRecipeListPanel().setRecipeListModel(newListModel);
  }

  private void displayRecipe(String recipeName) {
    Recipe recipe = new Recipe(); // gets set to the selected recipe
    for (Recipe r : rm.getRecipeList()) {
      // search list for the selected recipe
      if (r.getRecipe() == recipeName) {
        recipe = r;
        break;
      }
    }
    view.getRecipePanel().setRecipeName(recipe.getRecipe());
    view.getRecipePanel().setRecipeType(recipe.getType());
    view.getRecipePanel().setIngredients(recipe.getRecipeIngredientList());
    view.getRecipePanel().setRecipeContent(recipe.getInstructions());

    c.show(view.getCardsPanel(), WindowGUI.RECIPE_PANEL_NAME);
  }

  /**
   * Shows the recipe selected to be edited.
   * 
   * @param recipeName the name of the recipe to be edited
   */
  private void editRecipe(String recipeName) {
    Recipe recipe = new Recipe(); // gets set to the selected recipe
    for (Recipe r : rm.getRecipeList()) {
      // search list for the selected recipe
      if (r.getRecipe() == recipeName) {
        recipe = r;
        break;
      }
    }

    view.getEditRecipePanel().setRecipeName(recipe.getRecipe());
    view.getEditRecipePanel().setCategory(recipe.getType());
    view.getEditRecipePanel().setIngredients(recipe.getRecipeIngredientList());
    view.getEditRecipePanel().setRecipeContent(recipe.getInstructions());

    c.show(view.getCardsPanel(), WindowGUI.EDIT_RECIPE_PANEL_NAME);
  }

  /**
   * Saves the content from the editRecipePanel to the RecipeManager.
   */
  private void saveEditedRecipe() {
    Recipe r = new Recipe();

    r.setRecipe(view.getEditRecipePanel().getRecipeNameField());
    r.setType(view.getEditRecipePanel().getCategoryField());
    ArrayList<Ingredient> tempIngList = new ArrayList<Ingredient>();
    for (String s : view.getEditRecipePanel().getIngredientsField()) {
      tempIngList.add(new Ingredient(s));
    }
    r.setRecipeIngredientList(tempIngList); // parse ingredients
    r.setInstructions(view.getEditRecipePanel().getRecipeContentField());

    try {
      rm.addRecipe(r);
    } catch (IOException e) {
    }
  }

  /**
   * Used to show an empty editRecipePanel, for initializing a recipe.
   */
  private void showEmptyRecipe() {
    // empty the edit recipe panel
    view.getEditRecipePanel().setRecipeName("");
    view.getEditRecipePanel().setCategory(0);
    view.getEditRecipePanel().setIngredients(new ArrayList<String>());
    view.getEditRecipePanel().setRecipeContent("text");

    c.show(view.getCardsPanel(), WindowGUI.EDIT_RECIPE_PANEL_NAME);
  }

  private void removeRecipe(String recipeName) {
    int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete recipe: " + recipeName + "?",
        "Select a Option...", JOptionPane.YES_NO_OPTION);
    if (response == JOptionPane.YES_OPTION) {
      Recipe recipe = new Recipe(); // gets set to the selected recipe
      for (Recipe r : rm.getRecipeList()) {
        if (r.getRecipe() == recipeName) {
          recipe = r;
          break;
        }
      }
      try {
        rm.deleteRecipe(recipe);
      } catch (IOException e) {
      }
      updateRecipeList();
    }
  }

  /**
   * Refreshes the GUI with the newest IngredientManager data.
   */
  private void updateIngredientList() {
    DefaultListModel<String> newListModel = new DefaultListModel<String>();
    for (Ingredient i : im.officialList) {
      newListModel.addElement(i.getIngredient()); // adds the string to the list model
    }
    view.getPantryPanel().setPantryListModel(newListModel);
  }
  
  /**
   * Prompts the user to create a new ingredient and updates the IngredientManager.
   */
  private void addIngredient() {
    String name = null;
    if ((name = JOptionPane.showInputDialog("Enter an ingredient:")) == null) {
      return;
    }

    im.addIngredient(new Ingredient(name));
    updateIngredientList();
  }

  private void deleteIngredient(String selection) {
    for (Ingredient i : im.officialList) {
      if (i.getIngredient() == selection) {
        im.deleteIngredient(i);
        updateIngredientList();
        return;
      }
    }
  }
}
