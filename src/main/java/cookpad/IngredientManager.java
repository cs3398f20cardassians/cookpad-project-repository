package cookpad;

import java.util.*;
import java.io.IOException;

/**
 * @author Allison Spitzenberger
 */
public class IngredientManager {
  public static ArrayList<Ingredient> officialList = new ArrayList<Ingredient>();

  public void addIngredient(Ingredient i) {
    officialList.add(i);
  }

  public void deleteIngredient(Ingredient i) {
    if (officialList.contains(i)) {
      officialList.remove(i);
    }
  }

  /**
   * Sorts list of ingredients in alphabetical order.
   */
  public static void sortIngredient() throws IOException {
    Collections.sort(officialList, Ingredient.IngNameComparator);
  }
}
