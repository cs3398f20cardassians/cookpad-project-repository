package cookpad;

import javax.swing.*;
import java.awt.Component;
import java.util.*;

/**
 * Provides the view of the list of ingredients.
 * 
 * @author Brandon Burtchell
 * @author tagarduno
 */
public class PantryGUI {
  // constants for the pantry panel's button names:
  public static final String DELETE_INGREDIENT_BUTTON_NAME = "Delete Ingredient";
  public static final String ADD_INGREDIENT_BUTTON_NAME = "Add Ingredient";

  // swing components
  private JPanel panel;
  private JLabel ingredientsLabel;
  private JList<String> pantryList;
  private DefaultListModel<String> pantryListModel;
  private JScrollPane scrollPane;
  private JButton addButton;
  private JButton deleteButton;
  private JButton returnHomeButton;

  public PantryGUI() {
    // initialize swing components
    panel = new JPanel();
    ingredientsLabel = new JLabel("Ingredients");
    pantryList = new JList<String>();
    pantryListModel = new DefaultListModel<String>();
    scrollPane = new JScrollPane();
    addButton = new JButton(ADD_INGREDIENT_BUTTON_NAME);
    deleteButton = new JButton(DELETE_INGREDIENT_BUTTON_NAME);
    returnHomeButton = new JButton(WindowGUI.RETURN_HOME_BUTTON_NAME);
    
    // panel setup:
    panel.setPreferredSize(WindowGUI.DEFAULT_SCREEN_SIZE);
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    // list setup:
    pantryList.setModel(pantryListModel);

    // scroll pane setup:
    scrollPane.setViewportView(pantryList);

    // setting fonts:
    ingredientsLabel.setFont(WindowGUI.LARGE_HEADER_FONT);
    pantryList.setFont(WindowGUI.BUTTON_FONT);
    addButton.setFont(WindowGUI.BUTTON_FONT);
    deleteButton.setFont(WindowGUI.BUTTON_FONT);
    returnHomeButton.setFont(WindowGUI.BUTTON_FONT);

    // align components:
    ingredientsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    scrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
    addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    deleteButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    returnHomeButton.setAlignmentX(Component.CENTER_ALIGNMENT);

    // add panel components:
    panel.add(ingredientsLabel);
    panel.add(scrollPane);
    panel.add(addButton);
    panel.add(deleteButton);
    panel.add(returnHomeButton);
  }

  public JPanel getPanel() {
    return panel;
  }

  public String getSelectedIngredient() {
    return pantryListModel.getElementAt(pantryList.getSelectedIndex());
  }

  public JButton getAddIngredientButton() {
    return addButton;
  }

  public JButton getDeleteIngredientButton() {
    return deleteButton;
  }

  public void setPantryListModel(DefaultListModel<String> newListModel) {
    pantryListModel = newListModel;
    pantryList.setModel(pantryListModel);
    panel.revalidate();
  }

  public JButton getReturnHomeButton() {
    return returnHomeButton;
  }

}