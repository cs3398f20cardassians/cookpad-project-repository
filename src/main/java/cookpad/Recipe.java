package cookpad;

import java.util.ArrayList;

/**
 * @author Connor Scott
 */
public class Recipe {
  private String name;
  private String type;
  private String instructions;
  private ArrayList<Ingredient> recipeIngredientList;

  Recipe() {
    this.name = "";
    this.type = "";
    this.instructions = "";
    this.recipeIngredientList = new ArrayList<Ingredient>();
  }

  Recipe(String name, String type, String instructions, ArrayList<Ingredient> recipeIngredientList) {
    this.name = name;
    this.type = type;
    this.instructions = instructions;
    this.recipeIngredientList = recipeIngredientList;
  }

  public void setRecipe(String newName) {
    this.name = newName;
  }

  public void setType(String newType) {
    this.type = newType;
  }

  public void setInstructions(String newInstructions) {
    this.instructions = newInstructions;
  }

  public void setRecipeIngredientList(ArrayList<Ingredient> recipeIngredientList) {
    this.recipeIngredientList = recipeIngredientList;
  }

  public ArrayList<String> getRecipeIngredientList() {
    ArrayList<String> ingredientNames = new ArrayList<String>();
    for (Ingredient i : recipeIngredientList) {
      ingredientNames.add(i.getIngredient());
    }
    return ingredientNames;
  }

  public String getRecipeIngredientListToString() {
    return recipeIngredientList.toString();
  }

  public String getRecipe() {
    return name;
  }

  public String getType() {
    return type;
  }

  public String getInstructions() {
    return instructions;
  }

  public String nametoString() {
    String name;
    name = this.getRecipe();
    return name.toLowerCase();
  }
}