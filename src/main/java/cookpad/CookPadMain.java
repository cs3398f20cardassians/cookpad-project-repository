package cookpad;

import javax.swing.*;

/**
 * CookPad's main application class.
 * 
 * @author Brandon Burtchell
 */
public class CookPadMain {
  public static CookPadController c;

  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        c = new CookPadController();
      }
    });
  }
}