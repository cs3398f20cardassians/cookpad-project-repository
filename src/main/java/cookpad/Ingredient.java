package cookpad;

import java.util.Comparator;

/**
 * @author Allison Spitzenberger
 */
public class Ingredient{
  private String name;

  Ingredient() {
    this.name = null;
  }

  Ingredient(String name) {
    this.name = name;
  }

  /**
   * Sets the ingredient name.
   * @param name
   */
  public void setIngredient(String name) {
    this.name = name;
  }

  /**
   * Returns the ingredient name.
   */
  public String getIngredient() {
    return name;
  }

  public String ingredientNametoString() {
    String name;
    name= this.getIngredient();
    return name.toLowerCase();
  }

  /**
   * Comparator to allow the student object to be sorted alphabetically by name.
   */
  public static Comparator<Ingredient> IngNameComparator= new Comparator<Ingredient>() {
    public int compare(Ingredient s1, Ingredient s2) {
      String ingred1= s1.getIngredient().toUpperCase();
      String ingred2= s2.getIngredient().toUpperCase();

      //ascending order
      return ingred1.compareTo(ingred2);
    }
  };
}

