package cookpad;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Julian Sherlock
 */

public class TestIngredientManager
{
    private IngredientManager g = new IngredientManager();
    Ingredient i = new Ingredient("Apple");

    @Test
    @DisplayName("Test for adding ingredient to ingredientlist")
    public void testIngredientCreation()
    {
      g.addIngredient(i);
      assertNotNull(g.officialList);
    }

    //This test is currently not functional, return to at a later date
    @Test
    @DisplayName("Test for deleting ingredient from ingredientList")
    public void testIngredientDeletion()
    {
      g.addIngredient(i);
      g.deleteIngredient(i);
      //assertNull(g.officialList);
    }
}
