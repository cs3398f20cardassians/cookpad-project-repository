package cookpad;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

/**
 * @author Julian Sherlock
 */
public class TestRecipe
{
  private ArrayList<Ingredient> testIngredientList = new ArrayList<Ingredient>();
  Recipe r = new Recipe("TestName", "TestType",
                        "TestInstructions",
                        testIngredientList);
  @Test
  @DisplayName("Test for recipe Creation")
  public void testRecipeCreation()
  {
    assertNotNull(r);
  }

  @Test
  @DisplayName("Test for recipe nametoString")
  public void testnametoString()
  {
    assertEquals(r.nametoString(), "testname");
  }

  @Test
  @DisplayName("Test for recipe setters")
  public void testRecipeSetters()
  {
    r.setRecipe("TestName2");
    assertEquals(r.getRecipe(), "TestName2");
    r.setType("TestType2");
    assertEquals(r.getType(), "TestType2");
    r.setInstructions("TestInstructions2");
    assertEquals(r.getInstructions(), "TestInstructions2");
  }
}
