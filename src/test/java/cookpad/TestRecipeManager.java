package cookpad;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.io.IOException;


/**
 * @author Julian Sherlock
 */
public class TestRecipeManager
{
  private static RecipeManager g = new RecipeManager();
  private ArrayList<Ingredient> testIngredientList = new ArrayList<Ingredient>();
  Recipe r = new Recipe("TestName", "TestType",
                        "TestInstructions",
                        testIngredientList);

  //This test was functional at the time of completion of sprint 3, further changes
  //post sprint completion have created issues
  /*
  @Test
  @DisplayName("Test for adding recipe to recipeList")
  public void testRecipeAddition()
  {
    try
    {
      g.addRecipe(r);
    } catch (IOException e) {}
    assertNotNull(g.getRecipeList());
  }
  */

  //This test is not functional
  /*
  @Test
  @DisplayName("Test for removing recipe from recipeList")
  public void testRecipeDeletion()
  {
    try
    {
      g.addRecipe(r);
    } catch (IOException e) {}
    try
    {
      g.deleteRecipe(r);
    } catch (IOException e) {}

    //updateRecipeList();
    //assertNull(g.getRecipeList());
  }
  */
}
