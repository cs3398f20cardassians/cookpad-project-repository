package cookpad;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * @author Julian Sherlock
 */

public class TestIngredient
{
  Ingredient i = new Ingredient("TestName");

  @Test
  @DisplayName("Test for ingredient Creation")
  public void testIngredientCreation()
  {
    assertNotNull(i);
  }

  @Test
  @DisplayName("Test for ingredient nametoString")
  public void testnametoString()
  {
    assertEquals(i.ingredientNametoString(), "testname");
  }

  @Test
  @DisplayName("Test for ingredient setter")
  public void testIngredientSetters()
  {
    i.setIngredient("TestName2");
    assertEquals(i.getIngredient(), "TestName2");
  }
}
